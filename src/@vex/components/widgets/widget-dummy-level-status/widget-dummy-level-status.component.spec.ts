import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetDummyLevelStatusComponent } from './widget-dummy-level-status.component';

describe('WidgetDummyLevelStatusComponent', () => {
  let component: WidgetDummyLevelStatusComponent;
  let fixture: ComponentFixture<WidgetDummyLevelStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetDummyLevelStatusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetDummyLevelStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
