import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetQuestBoardComponent } from './widget-quest-board.component';

describe('WidgetQuestBoardComponent', () => {
  let component: WidgetQuestBoardComponent;
  let fixture: ComponentFixture<WidgetQuestBoardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetQuestBoardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetQuestBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
