import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetPartyStatusComponent } from './widget-party-status.component';

describe('WidgetPartyStatusComponent', () => {
  let component: WidgetPartyStatusComponent;
  let fixture: ComponentFixture<WidgetPartyStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetPartyStatusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetPartyStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
