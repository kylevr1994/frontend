import { Component, OnInit } from '@angular/core';
import icFacebook from '@iconify/icons-fa-brands/facebook';
import icInstagram from '@iconify/icons-fa-brands/instagram';
import icTwitter from '@iconify/icons-fa-brands/twitter';
import icDiscord from '@iconify/icons-fa-brands/discord';
import icYoutube from '@iconify/icons-fa-brands/windows';
import { Icon } from '@visurel/iconify-angular';
import icAdd from '@iconify/icons-ic/twotone-Add';
import {MatIconModule} from '@angular/material/icon';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';


export interface PeriodicElement {
  platform: string;
  addButton: string;
  icon: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {platform: 'Health', addButton: 'connect', icon: 'Health'},
  {platform: 'Stamina', addButton: 'connect', icon: 'Stamina'},
  {platform: 'Strength', addButton: 'connect', icon: 'Strength'},
  {platform: 'Intelligence', addButton: 'connect', icon: 'Intelligence'},

];

@Component({
  selector: 'vex-widget-party-status',
  templateUrl: './widget-party-status.component.html',
  styleUrls: ['./widget-party-status.component.scss']
})
export class WidgetPartyStatusComponent implements OnInit {

  displayedColumns: string[] = ['icon','platform', 'addButton'];
  dataSource = ELEMENT_DATA;

  faCoffee = faCoffee;
  icFacebook = icFacebook;
  icTwitter = icTwitter;
  icInstagram = icInstagram;
  icDiscord = icDiscord;
  icYoutube = icYoutube;
  icAdd = icAdd;

  constructor() { }

  ngOnInit(): void {
  }

}
