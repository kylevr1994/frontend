import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetPlayerLocationComponent } from './widget-player-location.component';

describe('WidgetPlayerLocationComponent', () => {
  let component: WidgetPlayerLocationComponent;
  let fixture: ComponentFixture<WidgetPlayerLocationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetPlayerLocationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetPlayerLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
