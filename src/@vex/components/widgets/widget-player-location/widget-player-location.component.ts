import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'vex-widget-player-location',
  templateUrl: './widget-player-location.component.html',
  styleUrls: ['./widget-player-location.component.scss']
})
export class WidgetPlayerLocationComponent implements OnInit {

  constructor() { }

  isVisible = true;
//   showAllLocations = true;


  public SetIsVisible(value){
    this.isVisible = value;
  }

//   public SetShowAllLocations(value){
//   this.showAllLocations = value;
//   alert(this.showAllLocations);
//   }

  ngOnInit(): void {
  }

}
