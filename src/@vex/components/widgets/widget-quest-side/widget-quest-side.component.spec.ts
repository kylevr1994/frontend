import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetQuestSideComponent } from './widget-quest-side.component';

describe('WidgetQuestSideComponent', () => {
  let component: WidgetQuestSideComponent;
  let fixture: ComponentFixture<WidgetQuestSideComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetQuestSideComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetQuestSideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
