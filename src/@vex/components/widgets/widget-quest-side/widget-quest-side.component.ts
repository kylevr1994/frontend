import { Component, OnInit } from '@angular/core';
import icFacebook from '@iconify/icons-fa-brands/facebook';
import icInstagram from '@iconify/icons-fa-brands/instagram';
import icTwitter from '@iconify/icons-fa-brands/twitter';
import icDiscord from '@iconify/icons-fa-brands/discord';
import icYoutube from '@iconify/icons-fa-brands/windows';
import { Icon } from '@visurel/iconify-angular';
import icAdd from '@iconify/icons-ic/twotone-Add';
import {MatIconModule} from '@angular/material/icon';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';


export interface PeriodicElement {
  increase: boolean[];
  description: string;
}

// money, health, stamina, strength, intelligence, love, crafting, work
const ELEMENT_DATA: PeriodicElement[] = [
  {increase: [false, true, false, false, false, false, false, false], description: 'Brush your teeth in the morning.'},
  {increase: [false, true, false, false, false, false, false, false], description: 'Brush your teeth in the evening.'},
  {increase: [false, false, false, false, false, true, false, false], description: 'Get some fresh air.'},
  {increase: [false, false, false, false, false, false, true, true], description: 'Pet the dog.'},
];

@Component({
  selector: 'vex-widget-quest-side',
  templateUrl: './widget-quest-side.component.html',
  styleUrls: ['./widget-quest-side.component.scss']
})
export class WidgetQuestSideComponent implements OnInit {

  displayedColumns: string[] = ['platform', 'addButton'];
  dataSource = ELEMENT_DATA;
  isSecondReward = false;

  faCoffee = faCoffee;
  icFacebook = icFacebook;
  icTwitter = icTwitter;
  icInstagram = icInstagram;
  icDiscord = icDiscord;
  icYoutube = icYoutube;
  icAdd = icAdd;

  constructor() { }

  ngOnInit(): void {
  }

}
