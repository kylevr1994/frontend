import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetLevelStatusComponent } from './widget-level-status.component';

describe('WidgetLevelStatusComponent', () => {
  let component: WidgetLevelStatusComponent;
  let fixture: ComponentFixture<WidgetLevelStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetLevelStatusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetLevelStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
