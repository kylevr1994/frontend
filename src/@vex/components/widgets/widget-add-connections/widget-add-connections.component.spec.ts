import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetAddConnectionsComponent } from './widget-add-connections.component';

describe('WidgetAddConnectionsComponent', () => {
  let component: WidgetAddConnectionsComponent;
  let fixture: ComponentFixture<WidgetAddConnectionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetAddConnectionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetAddConnectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
