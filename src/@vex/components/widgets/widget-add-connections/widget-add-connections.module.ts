import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetAddConnectionsComponent } from './widget-add-connections.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { IconModule } from '@visurel/iconify-angular';
import { MatCardModule } from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';



@NgModule({
  declarations: [WidgetAddConnectionsComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    IconModule,
    MatCardModule,
    MatGridListModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule
  ],
  exports: [WidgetAddConnectionsComponent]
})
export class WidgetAddConnectionsModule { }
