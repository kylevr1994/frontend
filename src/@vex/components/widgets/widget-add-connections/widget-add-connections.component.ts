import { Component, OnInit, Input } from '@angular/core';
import icFacebook from '@iconify/icons-fa-brands/facebook';
import icInstagram from '@iconify/icons-fa-brands/instagram';
import icTwitter from '@iconify/icons-fa-brands/twitter';
import icDiscord from '@iconify/icons-fa-brands/discord';
import icYoutube from '@iconify/icons-fa-brands/youtube';
import { Icon } from '@visurel/iconify-angular';
import icAdd from '@iconify/icons-ic/twotone-Add';

export interface PeriodicElement {
  platform: string;
  addButton: string;
  icon: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {platform: 'Facebook', addButton: 'connect', icon: 'icFacebook'},
  {platform: 'Instagram', addButton: 'connect', icon: 'icInstagram'},
  {platform: 'Twitter', addButton: 'connect', icon: 'icTwitter'},
  {platform: 'Discord', addButton: 'connect', icon: 'icDiscord'},
  {platform: 'Youtube', addButton: 'connect', icon: 'icYoutube'},
];

@Component({
  selector: 'vex-widget-add-connections',
  templateUrl: './widget-add-connections.component.html',
  styleUrls: ['./widget-add-connections.component.scss']
})
export class WidgetAddConnectionsComponent implements OnInit {

  @Input() iconClass: string;

  displayedColumns: string[] = ['icon','platform', 'addButton'];
  dataSource = ELEMENT_DATA;

  icFacebook = icFacebook;
  icTwitter = icTwitter;
  icInstagram = icInstagram;
  icDiscord = icDiscord;
  icYoutube = icYoutube;
  icAdd = icAdd;

  constructor() { }

  ngOnInit(): void {
  }

}
