import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetAllLocationsComponent } from './widget-all-locations.component';

describe('WidgetAllLocationsComponent', () => {
  let component: WidgetAllLocationsComponent;
  let fixture: ComponentFixture<WidgetAllLocationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetAllLocationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetAllLocationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
