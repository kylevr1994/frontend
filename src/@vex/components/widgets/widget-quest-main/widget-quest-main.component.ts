import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import icFacebook from '@iconify/icons-fa-brands/facebook';
import icInstagram from '@iconify/icons-fa-brands/instagram';
import icTwitter from '@iconify/icons-fa-brands/twitter';
import icDiscord from '@iconify/icons-fa-brands/discord';
import icYoutube from '@iconify/icons-fa-brands/windows';
import { Icon } from '@visurel/iconify-angular';
import icAdd from '@iconify/icons-ic/twotone-Add';
import {MatIconModule} from '@angular/material/icon';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';


export interface PeriodicElement {
  increase: boolean[];
  description: string;
  icon: string;
}

// money, health, stamina, strength, intelligence, love, crafting, work
const ELEMENT_DATA: PeriodicElement[] = [
  {increase: [false, false, true, true, false, false, false, false], description: 'If we want to be able to defeat the gaints, we need to workout. So go to the gym and get stronger.', icon: 'Health'},
  {increase: [false, true, false, false, false, false, true, false], description: 'I want you to live a healthy life and try new things. Can you cook something you haven\'t had before?', icon: 'Stamina'},
  {increase: [false, false, false, false, false, true, false, false], description: 'You haven\'t seen your family in a while. It\'s time to go visit them. We can handle it here for now.' , icon: 'Strength'},
  {increase: [false, false, false, false, false, false, true, true], description: 'The camp is a mess! Can you help me clean it? You only have to do it for 10 minutes.', icon: 'Intelligence'},

];

@Component({
  selector: 'vex-widget-quest-main',
  templateUrl: './widget-quest-main.component.html',
  styleUrls: ['./widget-quest-main.component.scss']
})
export class WidgetQuestMainComponent implements OnInit {

  displayedColumns: string[] = ['icon','platform', 'addButton'];
  dataSource = null;
  isSecondReward = false;

  faCoffee = faCoffee;
  icFacebook = icFacebook;
  icTwitter = icTwitter;
  icInstagram = icInstagram;
  icDiscord = icDiscord;
  icYoutube = icYoutube;
  icAdd = icAdd;



  constructor(
    private router: Router,
    private fb: FormBuilder,
    private http: HttpClient,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
  this.send();
  }

  public GetRewardAmount(): boolean{
      return this.isSecondReward;
  }


  public AddRewardAmount() {
  this.isSecondReward = true;
  }

  public send() {
    const headers = { 'Authorization': 'Bearer my-token'};
    const body = { 'email': localStorage.getItem('rememberMeEmail') };
    this.http.post<any>('http://localhost:8080/game/quests', body, { headers }).subscribe(data => {
        if(data){
        this.dataSource = data;
        data.forEach(function (value) {
         console.log(value);
        });
        }
    });
  }

}
