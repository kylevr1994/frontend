import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetQuestMainComponent } from './widget-quest-main.component';

describe('WidgetQuestMainComponent', () => {
  let component: WidgetQuestMainComponent;
  let fixture: ComponentFixture<WidgetQuestMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetQuestMainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetQuestMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
