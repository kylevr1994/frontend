import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetAddProductComponent } from './widget-add-product.component';

describe('WidgetAddProductComponent', () => {
  let component: WidgetAddProductComponent;
  let fixture: ComponentFixture<WidgetAddProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetAddProductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetAddProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
