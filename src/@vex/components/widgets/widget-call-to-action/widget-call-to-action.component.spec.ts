import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetCallToActionComponent } from './widget-call-to-action.component';

describe('WidgetCallToActionComponent', () => {
  let component: WidgetCallToActionComponent;
  let fixture: ComponentFixture<WidgetCallToActionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetCallToActionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetCallToActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
