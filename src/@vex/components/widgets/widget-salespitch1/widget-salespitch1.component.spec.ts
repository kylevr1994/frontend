import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetSalespitch1Component } from './widget-salespitch1.component';

describe('WidgetSalespitch1Component', () => {
  let component: WidgetSalespitch1Component;
  let fixture: ComponentFixture<WidgetSalespitch1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetSalespitch1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetSalespitch1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
