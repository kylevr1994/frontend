import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { IconModule } from '@visurel/iconify-angular';
import { MatCardModule } from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import { MatTableModule } from '@angular/material/table';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

import { WidgetHowtoplayComponent } from './widget-howtoplay.component';



@NgModule({
  declarations: [WidgetHowtoplayComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    IconModule,
    MatCardModule,
    MatGridListModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatProgressBarModule
  ],
  exports: [WidgetHowtoplayComponent]
})
export class WidgetHowtoplayModule { }
