import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetHowtoplayComponent } from './widget-howtoplay.component';

describe('WidgetHowtoplayComponent', () => {
  let component: WidgetHowtoplayComponent;
  let fixture: ComponentFixture<WidgetHowtoplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetHowtoplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetHowtoplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
