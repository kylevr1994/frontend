import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from './layout/layout.module';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions } from '@angular/material/form-field';
import { WidgetAddProductComponent } from './components/widgets/widget-add-product/widget-add-product.component';
import { WidgetAddConnectionsComponent } from './components/widgets/widget-add-connections/widget-add-connections.component';
import { WidgetLevelStatusComponent } from './components/widgets/widget-level-status/widget-level-status.component';
import { WidgetPartyStatusComponent } from './components/widgets/widget-party-status/widget-party-status.component';
import { WidgetPlayerLocationComponent } from './components/widgets/widget-player-location/widget-player-location.component';
import { WidgetQuestBoardComponent } from './components/widgets/widget-quest-board/widget-quest-board.component';
import { WidgetAllLocationsComponent } from './components/widgets/widget-all-locations/widget-all-locations.component';
import { WidgetQuestSideComponent } from './components/widgets/widget-quest-side/widget-quest-side.component';
import { WidgetQuestMainComponent } from './components/widgets/widget-quest-main/widget-quest-main.component';
import { LandingheaderComponent } from './components/landingheader/landingheader.component';
import { WidgetHowtoplayComponent } from './components/widgets/widget-howtoplay/widget-howtoplay.component';
import { WidgetDummyLevelStatusComponent } from './components/widgets/widget-dummy-level-status/widget-dummy-level-status.component';
import { WidgetSalespitch1Component } from './components/widgets/widget-salespitch1/widget-salespitch1.component';
import { WidgetCallToActionComponent } from './components/widgets/widget-call-to-action/widget-call-to-action.component';


@NgModule({
  imports: [
    CommonModule,
    LayoutModule
  ],
  exports: [
    LayoutModule
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'fill'
      } as MatFormFieldDefaultOptions
    }
  ],
  declarations: [
  ]
})
export class VexModule {
}
