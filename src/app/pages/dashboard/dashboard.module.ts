import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';

import { SecondaryToolbarModule } from '../../../@vex/components/secondary-toolbar/secondary-toolbar.module';
import { BreadcrumbsModule } from '../../../@vex/components/breadcrumbs/breadcrumbs.module';
import { IconModule } from '@visurel/iconify-angular';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ContainerModule } from '../../../@vex/directives/container/container.module';
import { PageLayoutModule } from '../../../@vex/components/page-layout/page-layout.module';
import { ChartModule } from '../../../@vex/components/chart/chart.module';

import { WidgetTableModule } from '../../../@vex/components/widgets/widget-table/widget-table.module';
import { WidgetAssistantModule } from '../../../@vex/components/widgets/widget-assistant/widget-assistant.module';
import { WidgetAddConnectionsModule } from '../../../@vex/components/widgets/widget-add-connections/widget-add-connections.module';
import { WidgetQuickLineChartModule } from '../../../@vex/components/widgets/widget-quick-line-chart/widget-quick-line-chart.module';
import { WidgetLargeChartModule } from '../../../@vex/components/widgets/widget-large-chart/widget-large-chart.module';
import { WidgetQuickValueStartModule } from '../../../@vex/components/widgets/widget-quick-value-start/widget-quick-value-start.module';
import { WidgetQuickValueCenterModule } from '../../../@vex/components/widgets/widget-quick-value-center/widget-quick-value-center.module';
import { WidgetLevelStatusModule } from '../../../@vex/components/widgets/widget-level-status/widget-level-status.module';
import { WidgetPartyStatusModule } from '../../../@vex/components/widgets/widget-party-status/widget-party-status.module';
import { WidgetPlayerLocationModule } from '../../../@vex/components/widgets/widget-player-location/widget-player-location.module';
import { WidgetAllLocationsModule } from '../../../@vex/components/widgets/widget-all-locations/widget-all-locations.module';
import { WidgetQuestBoardModule } from '../../../@vex/components/widgets/widget-quest-board/widget-quest-board.module';

import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import {MatProgressBarModule} from '@angular/material/progress-bar';



@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SecondaryToolbarModule,
    WidgetLargeChartModule,
    BreadcrumbsModule,
    MatIconModule,
    IconModule,
    ContainerModule,
    FlexLayoutModule,
    WidgetQuickValueStartModule,
    WidgetAssistantModule,
    WidgetQuickLineChartModule,
    WidgetQuickValueCenterModule,
    PageLayoutModule,
    ChartModule,
    MatButtonModule,
    WidgetTableModule,
    WidgetAddConnectionsModule,
    WidgetAddConnectionsModule,
    WidgetLevelStatusModule,
    WidgetPartyStatusModule,
    WidgetPlayerLocationModule,
    WidgetAllLocationsModule,
    WidgetQuestBoardModule,
    MatProgressBarModule
  ]
})
export class DashboardModule { }
