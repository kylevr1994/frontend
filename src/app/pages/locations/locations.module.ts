import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocationsComponent } from './locations.component';
import { LocationsRoutingModule } from './locations-routing.module';

import { SecondaryToolbarModule } from '../../../@vex/components/secondary-toolbar/secondary-toolbar.module';
import { BreadcrumbsModule } from '../../../@vex/components/breadcrumbs/breadcrumbs.module';
import { IconModule } from '@visurel/iconify-angular';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ContainerModule } from '../../../@vex/directives/container/container.module';
import { PageLayoutModule } from '../../../@vex/components/page-layout/page-layout.module';
import { ChartModule } from '../../../@vex/components/chart/chart.module';

import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

import { WidgetPlayerLocationModule } from '../../../@vex/components/widgets/widget-player-location/widget-player-location.module';
import { WidgetAllLocationsModule } from '../../../@vex/components/widgets/widget-all-locations/widget-all-locations.module';



@NgModule({
  declarations: [LocationsComponent],
  imports: [
    CommonModule,
    LocationsRoutingModule,
    SecondaryToolbarModule,
    BreadcrumbsModule,
    IconModule,
    FlexLayoutModule,
    ContainerModule,
    PageLayoutModule,
    ChartModule,
    MatIconModule,
    MatButtonModule,
    WidgetPlayerLocationModule,
    WidgetAllLocationsModule
  ]
})
export class LocationsModule { }
