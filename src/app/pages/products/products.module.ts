import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products.component';
import { ProductsRoutingModule } from './products-routing.module';

import { WidgetTableModule } from '../../../@vex/components/widgets/widget-table/widget-table.module';
import { WidgetAssistantModule } from '../../../@vex/components/widgets/widget-assistant/widget-assistant.module';
import { WidgetQuickLineChartModule } from '../../../@vex/components/widgets/widget-quick-line-chart/widget-quick-line-chart.module';
import { WidgetLargeChartModule } from '../../../@vex/components/widgets/widget-large-chart/widget-large-chart.module';

import { SecondaryToolbarModule } from '../../../@vex/components/secondary-toolbar/secondary-toolbar.module';
import { BreadcrumbsModule } from '../../../@vex/components/breadcrumbs/breadcrumbs.module';
import { IconModule } from '@visurel/iconify-angular';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ContainerModule } from '../../../@vex/directives/container/container.module';
import { PageLayoutModule } from '../../../@vex/components/page-layout/page-layout.module';
import { ChartModule } from '../../../@vex/components/chart/chart.module';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [ProductsComponent],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    WidgetTableModule,
    SecondaryToolbarModule,
    BreadcrumbsModule,
    IconModule,
    WidgetAssistantModule,
    WidgetQuickLineChartModule,
    WidgetLargeChartModule,
    FlexLayoutModule,
    ContainerModule,
    PageLayoutModule,
    ChartModule,
    MatButtonModule,
    MatIconModule,

  ]
})
export class ProductsModule { }
