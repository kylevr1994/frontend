import { Component, OnInit, Inject  } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import icDelete from '@iconify/icons-ic/twotone-delete';


interface User {
  name: string;
  email: string;
  iconUrl: string; // URL to the user's icon
  canEditTeam: boolean;
}

@Component({
  selector: 'vex-teamview',
  templateUrl: './teamview.component.html',
  styleUrls: ['./teamview.component.scss']
})
export class TeamviewComponent implements OnInit {

  users: User[] = [
    {
      name: 'Panna Hed',
      email: 'user1@example.com',
      iconUrl: 'assets/img/characters/1.png',
      canEditTeam: true
    },
    {
      name: 'Adrasteia Gertie',
      email: 'user2@example.com',
      iconUrl: 'assets/img/characters/2.png',
      canEditTeam: false
    },
    {
      name: 'Salomé Audaweniz',
      email: 'user1@example.com',
      iconUrl: 'assets/img/characters/3.png',
      canEditTeam: false
    },
    {
      name: 'Panna Hed',
      email: 'user1@example.com',
      iconUrl: 'assets/img/characters/1.png',
      canEditTeam: true
    },
    {
      name: 'Adrasteia Gertie',
      email: 'user2@example.com',
      iconUrl: 'assets/img/characters/2.png',
      canEditTeam: false
    },
    {
      name: 'Salomé Audaweniz',
      email: 'user1@example.com',
      iconUrl: 'assets/img/characters/3.png',
      canEditTeam: false
    },
    {
      name: 'Hlíf Arlotto',
      email: 'user2@example.com',
      iconUrl: 'assets/img/characters/4.png',
      canEditTeam: false
    }
  ];

  isNew = false;
  canEdit = false;
  icDelete = icDelete;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.isNew = this.data.isNew;
    this.canEdit = this.data.canEdit;
  }

  save(){

  }

}
