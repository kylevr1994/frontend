import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamcodeComponent } from './teamcode.component';

describe('TeamcodeComponent', () => {
  let component: TeamcodeComponent;
  let fixture: ComponentFixture<TeamcodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamcodeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamcodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
