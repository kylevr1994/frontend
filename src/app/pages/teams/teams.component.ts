import { Component, OnInit } from '@angular/core';
import { stagger80ms } from '../../../@vex/animations/stagger.animation';
import { fadeInUp400ms } from '../../../@vex/animations/fade-in-up.animation';
import icDelete from '@iconify/icons-ic/twotone-delete';
import { MaterialIcon } from 'material-icons';
import { MatDialog } from '@angular/material/dialog';
import { TeamcodeComponent } from './teamcode/teamcode.component';
import { TeamviewComponent } from './teamview/teamview.component';
import { AuthService } from 'src/app/auth/auth.service';


interface User {
  name: string;
  email: string;
  iconUrl: string; // URL to the user's icon
}

interface Team {
  name: string;
  email: string;
  iconUrl: string; // URL to the user's icon
}



@Component({
  selector: 'vex-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss'],
  animations: [
    stagger80ms,
    fadeInUp400ms
  ]
})
export class TeamsComponent implements OnInit {

  constructor(private dialog: MatDialog, private authservice: AuthService) { }

  icDelete = icDelete;
  userRight = 3;
  

  ngOnInit(): void {
    this.userRight = this.authservice.getUserRight();
    console.log(this.userRight);
  }

  teams: Team[] = [
    {
      name: 'Team Rocket',
      email: 'Development',
      iconUrl: 'assets/img/teams/1.png'
    },
    {
      name: 'Retail NL',
      email: 'Business',
      iconUrl: 'assets/img/teams/8.png'
    },
    {
      name: 'Starbreeze',
      email: 'Development',
      iconUrl: 'assets/img/teams/empty.png'
    }
  ]

  users: User[] = [
    {
      name: 'NetWatch',
      email: 'Security',
      iconUrl: 'assets/img/teams/6.png'
    },
    {
      name: 'Team Panda',
      email: 'Development',
      iconUrl: 'assets/img/teams/2.png'
    },
    {
      name: 'Corpo',
      email: 'Development',
      iconUrl: 'assets/img/teams/5.png'
    },
    {
      name: 'Team Nomad',
      email: 'Business',
      iconUrl: 'assets/img/teams/4.png'
    },
    {
      name: 'Arasaka',
      email: 'Development',
      iconUrl: 'assets/img/teams/3.png'
    },
    {
      name: 'Retail India',
      email: 'Development',
      iconUrl: 'assets/img/teams/7.png'
    }
  ];

  searchTerm: string = '';
  filteredUsers: User[] = this.users;

  onSearch() {
    this.filteredUsers = this.users.filter(user =>
      user.name.toLowerCase().includes(this.searchTerm.toLowerCase()) ||
      user.email.toLowerCase().includes(this.searchTerm.toLowerCase())
    );
  }

  clearSearch() {
    this.searchTerm = '';
    this.filteredUsers = this.users;
  }

  view() {
    this.dialog.open(TeamviewComponent, {
      data: {
        isNew: false,
        canEdit: false
      },
      width: '700px',
      maxWidth: '100%',
      disableClose: true
    });
  }

  createTeam() {
    this.dialog.open(TeamviewComponent, {
      data: {
        isNew: true,
        canEdit: true
      },
      width: '700px',
      maxWidth: '100%',
      disableClose: true
    });
  }

  

  open() {
    this.dialog.open(TeamcodeComponent, {
      //data: { card, list, board },
      width: '700px',
      maxWidth: '100%',
      disableClose: true
    });
    // .beforeClosed().pipe(
    //   filter<ScrumboardCard>(Boolean)
    // ).subscribe(value => {
    //   const index = list.children.findIndex(child => child.id === card.id);
    //   if (index > -1) {
    //     list.children[index] = value;
    //   }
    // });

  }

}
