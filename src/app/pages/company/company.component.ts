import { Component, OnInit } from '@angular/core';
import { stagger80ms } from '../../../@vex/animations/stagger.animation';
import icDelete from '@iconify/icons-ic/twotone-delete';
import { fadeInUp400ms } from '../../../@vex/animations/fade-in-up.animation';
import { AuthService } from 'src/app/auth/auth.service';

interface User {
  name: string;
  email: string;
  iconUrl: string; // URL to the user's icon
  userRight: number;
}


@Component({
  selector: 'vex-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss'],
  animations: [
    stagger80ms,
    fadeInUp400ms
  ]
})
export class CompanyComponent implements OnInit {

  icDelete = icDelete;
  userRight = 3;

  constructor( private authservice: AuthService) { }


  

  ngOnInit(): void {
    this.userRight = this.authservice.getUserRight();
  }
  users: User[] = [
    {
      name: 'Panna Hed',
      email: 'user1@example.com',
      iconUrl: 'assets/img/characters/1.png',
      userRight: 3
    },
    {
      name: 'Adrasteia Gertie',
      email: 'user2@example.com',
      iconUrl: 'assets/img/characters/2.png',
      userRight: 3
    },
    {
      name: 'Salomé Audaweniz',
      email: 'user1@example.com',
      iconUrl: 'assets/img/characters/3.png',
      userRight: 1
    },
    {
      name: 'Hlíf Arlotto',
      email: 'user2@example.com',
      iconUrl: 'assets/img/characters/4.png',
      userRight: 0
    },
    {
      name: 'Olívia Ai',
      email: 'user1@example.com',
      iconUrl: 'assets/img/characters/1.png',
      userRight: 1
    },
    {
      name: 'Germanicus Godric',
      email: 'user2@example.com',
      iconUrl: 'assets/img/characters/2.png',
      userRight: 2
    },
    {
      name: 'Tarasios Patrick',
      email: 'user1@example.com',
      iconUrl: 'assets/img/characters/3.png',
      userRight: 3
    },
    {
      name: 'Abdullaahi Raja',
      email: 'user2@example.com',
      iconUrl: 'assets/img/characters/4.png',
      userRight: 3
    },
    {
      name: 'Mukta Xander',
      email: 'user2@example.com',
      iconUrl: 'assets/img/characters/1.png',
      userRight: 2
    },
    {
      name: 'Atanasio Romilius',
      email: 'user1@example.com',
      iconUrl: 'assets/img/characters/2.png',
      userRight: 3
    },
    {
      name: 'Umukoro Alfons',
      email: 'user2@example.com',
      iconUrl: 'assets/img/characters/3.png',
      userRight: 3
    },
    {
      name: 'Catarina Eldad',
      email: 'user1@example.com',
      iconUrl: 'assets/img/characters/4.png',
      userRight: 3
    },
    {
      name: 'Amenemhat Wigmund',
      email: 'user2@example.com',
      iconUrl: 'assets/img/characters/1.png',
      userRight: 3
    },
    {
      name: 'Marie-France Cepheus',
      email: 'user2@example.com',
      iconUrl: 'assets/img/characters/2.png',
      userRight: 3
    },
    {
      name: 'Mohammad Rosalia',
      email: 'user1@example.com',
      iconUrl: 'assets/img/characters/3.png',
      userRight: 3
    },
    {
      name: 'Kyle van Raaij',
      email: 'user2@example.com',
      iconUrl: 'assets/img/characters/4.png',
      userRight: 3
    },
    {
      name: 'Adir van Raaij',
      email: 'contact@example.com',
      iconUrl: 'assets/img/characters/1.png',
      userRight: 3
    },
    {
      name: 'Johannes Sanela',
      email: 'user2@example.com',
      iconUrl: 'assets/img/characters/2.png',
      userRight: 3
    },
    // Add more user objects as needed
  ];

  searchTerm: string = '';
  filteredUsers: User[] = this.users;

  onSearch() {
    this.filteredUsers = this.users.filter(user =>
      user.name.toLowerCase().includes(this.searchTerm.toLowerCase()) ||
      user.email.toLowerCase().includes(this.searchTerm.toLowerCase())
    );
  }

  clearSearch() {
    this.searchTerm = '';
    this.filteredUsers = this.users;
  }

}
