import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyComponent } from './company.component';
import { SecondaryToolbarModule } from '../../../@vex/components/secondary-toolbar/secondary-toolbar.module';
import { MatCardModule } from '@angular/material/card';
import {MatExpansionModule} from '@angular/material/expansion';
import { IconModule } from '@visurel/iconify-angular';
import { BreadcrumbsModule } from '../../../@vex/components/breadcrumbs/breadcrumbs.module';
import { WidgetAddConnectionsModule } from '../../../@vex/components/widgets/widget-add-connections/widget-add-connections.module';
import { WidgetLevelStatusModule } from '../../../@vex/components/widgets/widget-level-status/widget-level-status.module';
import { WidgetPartyStatusModule } from '../../../@vex/components/widgets/widget-party-status/widget-party-status.module';
import { WidgetPlayerLocationModule } from '../../../@vex/components/widgets/widget-player-location/widget-player-location.module';
import { WidgetAllLocationsModule } from '../../../@vex/components/widgets/widget-all-locations/widget-all-locations.module';
import { WidgetQuestBoardModule } from '../../../@vex/components/widgets/widget-quest-board/widget-quest-board.module';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

import { FlexLayoutModule } from '@angular/flex-layout';
import { ContainerModule } from '../../../@vex/directives/container/container.module';
import { PageLayoutModule } from '../../../@vex/components/page-layout/page-layout.module';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FormsModule } from '@angular/forms';




@NgModule({
  declarations: [CompanyComponent],
  imports: [
    CommonModule,
    SecondaryToolbarModule,
    MatCardModule,
    MatExpansionModule,
    MatIconModule,
    FormsModule,
    IconModule,
    BreadcrumbsModule,
    MatSlideToggleModule,
    MatButtonModule,
    FlexLayoutModule,
    ContainerModule,
    PageLayoutModule,
    WidgetAddConnectionsModule,
    WidgetLevelStatusModule,
    WidgetPartyStatusModule,
    WidgetPlayerLocationModule,
    WidgetQuestBoardModule,
    MatProgressBarModule,
    WidgetAllLocationsModule,
    MatInputModule,
    MatFormFieldModule
  ]
})
export class CompanyModule { }
