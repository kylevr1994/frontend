import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecoverCodeComponent } from './recover-code.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { IconModule } from '@visurel/iconify-angular';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RecoverCodeRoutingModule } from './recover-code-routing.module';


@NgModule({
  declarations: [RecoverCodeComponent],
  imports: [
    CommonModule,
    RecoverCodeRoutingModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    IconModule,
    MatIconModule,
    MatSnackBarModule
  ],
  exports: [RecoverCodeComponent]
})
export class RecoverCodeModule { }
