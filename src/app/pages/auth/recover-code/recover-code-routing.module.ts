import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuicklinkModule } from 'ngx-quicklink';
import { RecoverCodeComponent } from './recover-code.component';


const routes: Routes = [
  {
    path: '',
    component: RecoverCodeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule, QuicklinkModule]
})
export class RecoverCodeRoutingModule { }