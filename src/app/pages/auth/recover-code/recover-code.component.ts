import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { fadeInUp400ms } from '../../../../@vex/animations/fade-in-up.animation';
import icMail from '@iconify/icons-ic/twotone-mail';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';

import icVisibility from '@iconify/icons-ic/twotone-visibility';
import icVisibilityOff from '@iconify/icons-ic/twotone-visibility-off';

@Component({
  selector: 'vex-recover-code',
  templateUrl: './recover-code.component.html',
  styleUrls: ['./recover-code.component.scss']
})
export class RecoverCodeComponent implements OnInit {

  @ViewChild("passwordfield") passwordfield;
  form: FormGroup;
  postId;
  errorMessage;
  isRememberMeChecked;

  inputType = 'password';
  visible = false;

  icVisibility = icVisibility;
  icVisibilityOff = icVisibilityOff;

  icMail = icMail;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private http: HttpClient,
    private cd: ChangeDetectorRef,
    private snackbar: MatSnackBar,
  ) { }

  ngOnInit(): void {
      this.form = this.fb.group({
        email: [localStorage.getItem('rememberMeEmail'), Validators.required],
        recover_code: ['', Validators.required],
        password: ['', Validators.required]
      });
  }

    send() {
      const headers = { 'Authorization': 'Bearer my-token', 'ResetCode' : this.form.get('recover_code').value, 'NewPassword' : this.form.get('password').value };
      const body = { 'email': this.form.get('email').value };
      this.http.post<any>('http://localhost:8080/auth/recover_code', body, { headers }).subscribe(data => {
          if(data){
            this.router.navigate(['/login']);
          }
          console.log(data);

          if(data == false){
            this.form.controls['recover_code'].setErrors({'wrong_credentials': true});
          }

          if(data == null) {
               this.snackbar.open('It looks like we have problems with our server. An engineer has been notified and will be working on a solution. Sorry for the inconvenience', 'OK THANKS', {
                 duration: 50000
               });
              return;
          }
      });
    }

      toggleVisibility() {
        if (this.visible) {
          this.inputType = 'password';
          this.visible = false;
          this.cd.markForCheck();
        } else {
          this.inputType = 'text';
          this.visible = true;
          this.cd.markForCheck();
        }
      }

}
