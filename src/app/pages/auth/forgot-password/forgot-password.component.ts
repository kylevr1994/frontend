import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { fadeInUp400ms } from '../../../../@vex/animations/fade-in-up.animation';
import icMail from '@iconify/icons-ic/twotone-mail';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'vex-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
  animations: [fadeInUp400ms]
})
export class ForgotPasswordComponent implements OnInit {

  form = this.fb.group({
    email: [null, Validators.required]
  });

  icMail = icMail;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private http: HttpClient,
    private snackbar: MatSnackBar,
  ) { }

  ngOnInit() {
  }

  send() {
    const headers = { 'Authorization': 'Bearer my-token', 'My-Custom-Header': 'foobar' };
    const body = { 'email': this.form.get('email').value };
    this.http.post<any>('http://localhost:8080/auth/recover_password', body, { headers }).subscribe(data => {
        if(data == null) {
             this.snackbar.open('It looks like we have problems with our server. An engineer has been notified and will be working on a solution. Sorry for the inconvenience', 'OK THANKS', {
               duration: 50000
             });
            return;
        }

        this.router.navigate(['/']);
    });
  }
}
