import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import icVisibility from '@iconify/icons-ic/twotone-visibility';
import icVisibilityOff from '@iconify/icons-ic/twotone-visibility-off';
import { fadeInUp400ms } from '../../../../@vex/animations/fade-in-up.animation';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'vex-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    fadeInUp400ms
  ]
})

export class LoginComponent implements OnInit {

  @ViewChild("passwordfield") passwordfield;
  form: FormGroup;
  postId;
  errorMessage;
  isRememberMeChecked;

  inputType = 'password';
  visible = false;

  icVisibility = icVisibility;
  icVisibilityOff = icVisibilityOff;

  constructor(private router: Router,
              private fb: FormBuilder,
              private cd: ChangeDetectorRef,
              private snackbar: MatSnackBar,
              private http: HttpClient
  ) {}

  ngOnInit() {
    this.form = this.fb.group({
      email: [localStorage.getItem('rememberMeEmail'), Validators.required],
      password: ['', Validators.required]
    });

    this.form.get('email').valueChanges.subscribe(val => {
        let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (!this.form.get('email').value.match(regexEmail)) {
          this.form.controls['email'].setErrors({'incorrect': true});
        }
      });

    if(localStorage.getItem('rememberMeEmail') != null) {
        this.isRememberMeChecked = true;
    } else {
        localStorage.removeItem('rememberMe');
        localStorage.removeItem('rememberMeEmail');
    }

  }

  toggleRememberMe(event) {
    if ( event.checked ) {
        if(localStorage.getItem('rememberMe') == null) {
            localStorage.setItem('rememberMe', 'true');
        }
    } else {
        if(localStorage.getItem('rememberMe') == 'true') {
            localStorage.removeItem('rememberMe');
            localStorage.removeItem('rememberMeEmail');
        }
    }
  }


  send() {

    if(localStorage.getItem('rememberMe') == 'true') {
        if(this.form.get('email').value != '' || this.form.get('email').value != null){
            localStorage.setItem('rememberMeEmail', this.form.get('email').value);
        }
    } else {
         localStorage.removeItem('rememberMeEmail');
    }

    const headers = { 'Authorization': 'Bearer my-token', 'My-Custom-Header': 'foobar' };
    const body = { 'email': this.form.get('email').value, 'password': this.form.get('password').value };
    this.http.post<any>('http://localhost:8080/auth/login', body, { headers }).subscribe(data => {
        if(data == null) {
            this.form.controls['password'].setErrors({'wrong_credentials': true});

            this.passwordfield.nativeElement.focus();
            return;
        } else {
            this.postId = data.id;
        }



        if(data.sessionkey != null) {
            localStorage.setItem('token', data.sessionKey);
            localStorage.setItem('userId', data.userId);
            this.router.navigate(['/']);
        } else {
         this.form.controls['email'].setErrors({'wrong_credentials': true});
        }
    });
  }

  toggleVisibility() {
    if (this.visible) {
      this.inputType = 'password';
      this.visible = false;
      this.cd.markForCheck();
    } else {
      this.inputType = 'text';
      this.visible = true;
      this.cd.markForCheck();
    }
  }
}
