import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import icVisibility from '@iconify/icons-ic/twotone-visibility';
import icVisibilityOff from '@iconify/icons-ic/twotone-visibility-off';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { fadeInUp400ms } from '../../../../@vex/animations/fade-in-up.animation';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'vex-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  animations: [
    fadeInUp400ms
  ]
})
export class RegisterComponent implements OnInit {

  @ViewChild("namefield") namefield;
  form: FormGroup;
  postId;
  errorMessage;
  name;
  email;
  password;
  passwordConfirm;
  accept;

  inputType = 'password';
  visible = false;

  icVisibility = icVisibility;
  icVisibilityOff = icVisibilityOff;

  constructor(private router: Router,
              private fb: FormBuilder,
              private cd: ChangeDetectorRef,
              private http: HttpClient,
              private snackbar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      passwordConfirm: ['', Validators.required],
    });

    this.form.get('email').valueChanges.subscribe(val => {
            let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (!this.form.get('email').value.match(regexEmail)) {
              this.form.controls['email'].setErrors({'incorrect': true});
            }
          });

    this.form.get('passwordConfirm').valueChanges.subscribe(val => {
            if(this.form.get('passwordConfirm').value != this.form.get('password').value) {
                this.form.controls['passwordConfirm'].setErrors({'required': true});
            }
          });

    this.form.get('password').valueChanges.subscribe(val => {
        if(this.form.get('password').value.length <= 8) {
            this.form.controls['password'].setErrors({'toShort': true});
        }
    });

  }



  send() {

    // Check if all the fields are filled.
    // TODO check name
    if(this.form.get('name').value == '') {
        this.form.controls['name'].setErrors({'required': true});
        this.namefield.nativeElement.focus();
        console.log('dssfdfdsfsdfd');
    }


    let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!this.form.get('email').value.match(regexEmail)) {
      this.form.controls['email'].setErrors({'incorrect': true});
    }

    // Check if the user accepts the terms and conditions


    // Check if the the password and passwordConfirm are the same
    if(this.form.get('passwordConfirm').value != this.form.get('password').value) {
        this.form.controls['passwordConfirm'].setErrors({'required': true});
        return;
    }

    // Set the correct data in the header/body
    const headers = {
        'Authorization': 'Bearer my-token',
        'My-Custom-Header': 'foobar'
    };

    const body = {
        'name': this.form.get('name').value,
        'email': this.form.get('email').value,
        'password': this.form.get('password').value,
    };

    // Send the data to the server
    this.http.post<any>('http://localhost:8080/auth/register', body, { headers }).subscribe(data => {
        if(data == null) {
            this.snackbar.open('It seems like you already have an account', 'Thank you', {
              duration: 10000
            });
            this.router.navigate(['/login']);
            return;
        } else {
            this.postId = data.id;
        }
        if(data.sessionkey != null) {
            localStorage.setItem('token', data.sessionkey);
            localStorage.setItem('userId', data.userid);
            this.router.navigate(['/']);
        } else {



            console.log("failed to create a new account");
        }
    });
  }


  toggleVisibility() {
    if (this.visible) {
      this.inputType = 'password';
      this.visible = false;
      this.cd.markForCheck();
    } else {
      this.inputType = 'text';
      this.visible = true;
      this.cd.markForCheck();
    }
  }
}
