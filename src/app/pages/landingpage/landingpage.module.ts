import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingpageComponent } from './landingpage.component'

import { SecondaryToolbarModule } from '../../../@vex/components/secondary-toolbar/secondary-toolbar.module';
import { BreadcrumbsModule } from '../../../@vex/components/breadcrumbs/breadcrumbs.module';
import { IconModule } from '@visurel/iconify-angular';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ContainerModule } from '../../../@vex/directives/container/container.module';
import { PageLayoutModule } from '../../../@vex/components/page-layout/page-layout.module';
import { ChartModule } from '../../../@vex/components/chart/chart.module';

import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

import { WidgetPlayerLocationModule } from '../../../@vex/components/widgets/widget-player-location/widget-player-location.module';
import { WidgetAllLocationsModule } from '../../../@vex/components/widgets/widget-all-locations/widget-all-locations.module';
import { WidgetHowtoplayModule } from '../../../@vex/components/widgets/widget-howtoplay/widget-howtoplay.module';
import { WidgetLevelStatusModule } from '../../../@vex/components/widgets/widget-level-status/widget-level-status.module';
import { WidgetDummyLevelStatusModule } from '../../../@vex/components/widgets/widget-dummy-level-status/widget-dummy-level-status.module';
import { WidgetSalespitch1Module } from '../../../@vex/components/widgets/widget-salespitch1/widget-salespitch1.module';
import { WidgetCallToActionModule } from '../../../@vex/components/widgets/widget-call-to-action/widget-call-to-action.module';
import { LandingheaderModule } from '../../../@vex/components/landingheader/landingheader.module';



@NgModule({
  declarations: [LandingpageComponent],
  imports: [
    CommonModule,
    SecondaryToolbarModule,
    BreadcrumbsModule,
    IconModule,
    FlexLayoutModule,
    ContainerModule,
    PageLayoutModule,
    ChartModule,
    MatIconModule,
    MatButtonModule,
    WidgetPlayerLocationModule,
    WidgetAllLocationsModule,
    LandingheaderModule,
    WidgetLevelStatusModule,
    WidgetHowtoplayModule,
    WidgetDummyLevelStatusModule,
    WidgetSalespitch1Module,
    WidgetCallToActionModule
  ],
  exports: [LandingpageComponent]
})
export class LandingpageModule { }
