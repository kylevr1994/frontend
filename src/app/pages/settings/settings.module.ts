import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsComponent } from './settings.component'
import { SecondaryToolbarModule } from '../../../@vex/components/secondary-toolbar/secondary-toolbar.module';
import { MatCardModule } from '@angular/material/card';


@NgModule({
  declarations: [SettingsComponent],
  imports: [
    CommonModule,
    SecondaryToolbarModule,
    MatCardModule
  ]
})
export class SettingsModule { }
