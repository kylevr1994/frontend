import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ScrumboardCard } from '../../interfaces/scrumboard-card.interface';
import { FormArray, FormBuilder, FormControl } from '@angular/forms';
import icAssignment from '@iconify/icons-ic/twotone-assignment';
import icAdd from '@iconify/icons-ic/twotone-add';
import { AIUsers, scrumboardLabels, scrumboardUsers } from '../../../../../static-data/scrumboard';
import icDescription from '@iconify/icons-ic/twotone-description';
import icClose from '@iconify/icons-ic/twotone-close';
import { ScrumboardList } from '../../interfaces/scrumboard-list.interface';
import { Scrumboard } from '../../interfaces/scrumboard.interface';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icImage from '@iconify/icons-ic/twotone-image';
import { ScrumboardAttachment } from '../../interfaces/scrumboard-attachment.interface';
import icAttachFile from '@iconify/icons-ic/twotone-attach-file';
import icInsertComment from '@iconify/icons-ic/twotone-insert-comment';
import { DateTime } from 'luxon';
import { ScrumboardComment } from '../../interfaces/scrumboard-comment.interface';
import icStar from '@iconify/icons-ic/twotone-star';
import { ScrumboardComponent } from '../../scrumboard.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Configuration, OpenAIApi } from "openai";


@Component({
  selector: 'vex-scrumboard-dialog',
  templateUrl: './scrumboard-dialog.component.html',
  styleUrls: ['./scrumboard-dialog.component.scss']
})
export class ScrumboardDialogComponent implements OnInit {

  form = this.fb.group({
    title: null,
    description: null,
    dueDate: null,
    cover: null,
    attachments: this.fb.array([]),
    comments: this.fb.array([]),
    users: [],
    labels: []
  });

  commentCtrl = new FormControl();

  icAssignment = icAssignment;
  icDescription = icDescription;
  icClose = icClose;
  icAdd = icAdd;
  icMoreVert = icMoreVert;
  icDelete = icDelete;
  icImage = icImage;
  icAttachFile = icAttachFile;
  icInsertComment = icInsertComment;
  icStar = icStar;

  users = scrumboardUsers;
  labels = scrumboardLabels;

  list: ScrumboardList;
  board: Scrumboard;
  card: ScrumboardCard;

  // Chat GPT 
  apiKey = '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';
  apiUrl = 'https://api.openai.com/v1/engines/davinci/completions';



  constructor(private dialogRef: MatDialogRef<ScrumboardDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data: {
                card: ScrumboardCard;
                list: ScrumboardList;
                board: Scrumboard;
              },
              private httpClient: HttpClient,
              private fb: FormBuilder) { }

  ngOnInit() {
    this.list = this.data.list;
    this.board = this.data.board;

    this.card = this.data.card;
    

    this.form.patchValue({
      title: this.card.title,
      description: this.card.description,
      dueDate: this.card.dueDate || null,
      cover: this.card.cover || null,
      users: this.card.users,
      labels: this.card.labels || []
    });

    this.form.setControl('attachments', this.fb.array(this.card.attachments || []));
    this.form.setControl('comments', this.fb.array(this.card.comments || []));
  }

  save() {
    this.dialogRef.close(this.form.value);
  }

  isImageExtension(extension: string) {
    return extension === 'jpg' || extension === 'png';
  }

  makeCover(attachment: ScrumboardAttachment) {
    this.form.get('cover').setValue(attachment);
  }

  isCover(attachment: ScrumboardAttachment) {
    return this.form.get('cover').value === attachment;
  }

  deletePost(){
    this.list.children = this.list.children.filter(child => child.id != this.card.id);
  }

  async generateActionPoint(){


    const descriptionText = this.form.get('description').value;
    
    const configuration = new Configuration({
      apiKey: this.apiKey,
    });
    const openai = new OpenAIApi(configuration);

    const completion = await openai.createCompletion({
      model: "text-davinci-003",
      prompt: "You are a work program that creates one concrete actionpoint for the text that is given to you. You respond in Dutch. You are given the following text:" + descriptionText,
      max_tokens: 2048,
      user: "System"
    });

    this.board.children[3].children.push({
      id: ScrumboardComponent.nextId++,
      title: "Title",
      description: completion.data.choices[0].text.replace(/^\s*\n/, ''),
      users: AIUsers
    });

    close();

    return;
  }

  isActionPointRow(){
    if(this.card.users[0].name == AIUsers[0].name){
      return true;
    } else {
      return false;
    }
  }

  remove(attachment: ScrumboardAttachment) {
    if (this.form.get('cover').value && attachment.id === this.form.get('cover').value.id) {
      this.form.get('cover').setValue(null);
    }

    this.form.setControl('attachments', this.fb.array(this.form.get('attachments').value.filter(a => a !== attachment)));
  }

  addComment() {
    if (!this.commentCtrl.value) {
      return;
    }

    const comments = this.form.get('comments') as FormArray;
    comments.push(new FormControl({
      from: {
        name: 'Company name',
        imageSrc: 'assets/img/avatars/10.jpeg'
      },
      message: this.commentCtrl.value,
      date: DateTime.local().minus({ seconds: 1 })
    } as ScrumboardComment));

    this.commentCtrl.setValue(null);
  }
}
