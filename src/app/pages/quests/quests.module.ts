import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuestsComponent } from './quests.component';
import { QuestsRoutingModule } from './quests-routing.module';

import { SecondaryToolbarModule } from '../../../@vex/components/secondary-toolbar/secondary-toolbar.module';
import { BreadcrumbsModule } from '../../../@vex/components/breadcrumbs/breadcrumbs.module';
import { IconModule } from '@visurel/iconify-angular';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ContainerModule } from '../../../@vex/directives/container/container.module';
import { PageLayoutModule } from '../../../@vex/components/page-layout/page-layout.module';
import { ChartModule } from '../../../@vex/components/chart/chart.module';

import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

import { WidgetQuestSideModule } from '../../../@vex/components/widgets/widget-quest-side/widget-quest-side.module';
import { WidgetQuestMainModule } from '../../../@vex/components/widgets/widget-quest-main/widget-quest-main.module';

@NgModule({
  declarations: [QuestsComponent],
  imports: [
    CommonModule,
    QuestsRoutingModule,
    SecondaryToolbarModule,
    BreadcrumbsModule,
    IconModule,
    FlexLayoutModule,
    ContainerModule,
    PageLayoutModule,
    ChartModule,
    MatIconModule,
    MatButtonModule,
    WidgetQuestSideModule,
    WidgetQuestMainModule
  ]
})
export class QuestsModule { }
