import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(public router: Router,
              private http: HttpClient) {}
  canActivate(): boolean {

    const token = localStorage.getItem('token');
    const userId = localStorage.getItem('userId');

    // check if there is a session token in the localStorage
    if(token == null || userId == null){
        this.router.navigate(['login']);
        return false;
    }

    const headers = { 'Authorization': token, 'UserId': userId};
    const body = { };
    this.http.post<any>('http://localhost:8080/auth/token_check', body, { headers }).subscribe(data => {
        if(!data) {
            // session token is expired
            this.router.navigate(['login']);
        }
    });

    // TODO: check if the session token is correct
    return true;
  }
}

// For more information about security go to this article:
// https://medium.com/@ryanchenkie_40935/angular-authentication-using-route-guards-bf7a4ca13ae3