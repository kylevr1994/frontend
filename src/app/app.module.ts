import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { VexModule } from '../@vex/vex.module';
import { HttpClientModule } from '@angular/common/http';
import { CustomLayoutModule } from './custom-layout/custom-layout.module';
import { MatIconModule } from '@angular/material/icon';
import { ProductsComponent } from './pages/products/products.component';
import { InvoicesComponent } from './pages/invoices/invoices.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AuthGuardService as AuthGuard } from './auth/auth-guard.service';
import { ManagementComponent } from './pages/management/management.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { QuestsComponent } from './pages/quests/quests.component';
import { LocationsComponent } from './pages/locations/locations.component';
import { RecoverCodeComponent } from './pages/auth/recover-code/recover-code.component';
import { LandingpageComponent } from './pages/landingpage/landingpage.component';
import { CompanyComponent } from './pages/company/company.component';
import { TeamsComponent } from './pages/teams/teams.component';
import { MatDialogModule } from '@angular/material/dialog';



@NgModule({
  declarations: [AppComponent, InvoicesComponent, ManagementComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatDialogModule,
    MatIconModule,
    // Vex
    VexModule,
    CustomLayoutModule,
    MatProgressBarModule
  ],
  providers: [ AuthGuard ],
  bootstrap: [AppComponent]
})
export class AppModule { }
