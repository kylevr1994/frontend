import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomLayoutComponent } from './custom-layout/custom-layout.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { ProductsComponent } from './pages/products/products.component';
import { InvoicesComponent } from './pages/invoices/invoices.component';
import { ManagementComponent } from './pages/management/management.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LandingpageComponent } from './pages/landingpage/landingpage.component';
import { QuestsComponent } from './pages/quests/quests.component';
import { LocationsComponent } from './pages/locations/locations.component';
import { AuthGuardService as AuthGuard } from './auth/auth-guard.service';
import { ScrumboardComponent } from './pages/scrumboard/scrumboard.component';
import { RecoverCodeComponent } from './pages/auth/recover-code/recover-code.component';
import { CompanyComponent } from './pages/company/company.component';
import { TeamsComponent } from './pages/teams/teams.component';


const routes: Routes = [
    {
       path: 'login',
       loadChildren: () => import('./pages/auth/login/login.module').then(m => m.LoginModule),
    },
    {
       path: 'register',
       loadChildren: () => import('./pages/auth/register/register.module').then(m => m.RegisterModule),
    },
    {
        path: 'forgot-password',
        loadChildren: () => import('./pages/auth/forgot-password/forgot-password.module').then(m => m.ForgotPasswordModule),
    },
    {
        path: 'recover-code',
        loadChildren: () => import('./pages/auth/recover-code/recover-code.module').then(m => m.RecoverCodeModule),
    },
    {
    path: '',
    component: CustomLayoutComponent,
    children: [
      {
        path: 'home',
        component: LandingpageComponent
      },
      {
        path: 'settings',
        component: SettingsComponent,
        //canActivate: [AuthGuard]
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        //canActivate: [AuthGuard]
      },
      {
        path: 'quests',
        component: QuestsComponent,
        //canActivate: [AuthGuard]
      },
      {
        path: 'locations',
        component: LocationsComponent,
        //canActivate: [AuthGuard]
      },
      {
        path: 'company',
        component: CompanyComponent,
        //canActivate: [AuthGuard]
      },
      {
        path: 'teams',
        component: TeamsComponent,
        //canActivate: [AuthGuard]
      },
      {
        path: 'socials',
        children: [
            {
              path: 'groups',
              loadChildren: () => import('./pages/scrumboard/scrumboard.module').then(m => m.ScrumboardModule),
              //canActivate: [AuthGuard],
              data: { toolbarShadowEnabled: true }
            }
        ]
      },
      {
        path: 'retrospective',
        loadChildren: () => import('./pages/scrumboard/scrumboard.module').then(m => m.ScrumboardModule),
        //canActivate: [AuthGuard]
      },
      {
        path: 'products',
        component: ProductsComponent,
        //canActivate: [AuthGuard]
      },
      {
        path: 'invoices',
        component: InvoicesComponent,
        //canActivate: [AuthGuard]
      },
      {
        path: 'home',
        component: LandingpageComponent,
        //canActivate: [AuthGuard]
      },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // preloadingStrategy: PreloadAllModules,
    scrollPositionRestoration: 'enabled',
    relativeLinkResolution: 'corrected',
    anchorScrolling: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
